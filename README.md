# demoTableView #

### What is this project for? ###

* Learn about tableViews with UiKit by Cornell AppDev project tutorials!
* Coding using view code

### What is Cornell AppDev? ###

Cornell AppDev is a project team at Cornell University dedicated to open-source mobile and web app development. Check us out below!

► Website: [clique aqui](http://www.cornellappdev.com/). 

► Facebook: [clique aqui](https://www.facebook.com/cornellappdev/). 

► Instagram: [clique aqui](https://www.instagram.com/cornellappdev/). 